namespace TMenos3.Azure.AzureFunctions.Demos.EmailSender
{
    using Microsoft.Azure.WebJobs;
    using Microsoft.Extensions.Logging;
    using SendGrid.Helpers.Mail;

    public static class EmailSenderQueue
    {
        [FunctionName("EmailSenderQueue")]
        [return: SendGrid(ApiKey = "SendGridApiKey",
                          From = "90.emmanuel@gmail.com",
                          Subject = "{Subject}",
                          To = "{To}",
                          Text = "{Body}")]
        public static SendGridMessage Run(
            [QueueTrigger("emails", Connection = "StorageConnectionString")]
                Email email,
            ILogger logger)
        {
            return new SendGridMessage();
        }
    }
}
